import ForgeUI, { AdminPage, Fragment, render, Text, TextField, Form, useState, Button, CheckboxGroup, Checkbox } from '@forge/ui';
import { storage } from '@forge/api';

storage.set('example-key', [ 'Hello', 'World' ]);

const App = () => {
  // useState is a UI kit hook we use to manage the form data in local state
  const [formState, setFormState] = useState(undefined);
  // Handles form submission, which is a good place to call APIs, or to set component state...
  const onSubmit = async (formData) => {
    /**
     * formData:
     * {
     *    username: 'Username',
     *    password: ['jira']
     * }
     */
    var username = "test";
    var password = "test";
  
    if (username === formData["username"] && password === formData["password"]) {
      console.log(password);
      console.log(username);

    }
    else {
      const wrong = () => {
        return <Text>Hello world!</Text>;
      };
      console.log("Your password or username might be wrong.");
      
    };

    setFormState(formData);
  };
  const logOut = () => {};


  // The array of additional buttons.
  // These buttons align to the right of the submit button.
  const actionButtons = [
    <Button text="Log out" onClick={logOut} />,
  ];
  const login = "login";

  return (
    <Fragment>
      <Form onSubmit={onSubmit} actionButtons={actionButtons} submitButtonText={login}>
        <TextField name="username" label="Username" placeholder="Username here"/>
        <TextField name="password" label="Password" placeholder="Password here"/>
      </Form>
      {formState && <Text>{JSON.stringify(formState)}</Text>}
      <Text></Text>
      <Text>Password forgotten?</Text>
      <Text>Click here</Text>
      
    </Fragment>
  );
};

const Choose = () => { 
  
  const [formState, setFormState] = useState(undefined);

  const onSubmit = async (formData) => {

    var languages = formData["products"];

    console.log(languages);

    setFormState(formData);

  };

  return (
  <Fragment>
    <Form onSubmit={onSubmit}>
      <CheckboxGroup label="Products" name="products">
        <Checkbox value="de" label="German" />
        <Checkbox value="en" label="English" />
        <Checkbox value="fr" label="French" />
        <Checkbox value="es" label="Spanish" />
        <Checkbox value="zh-Hans" label="Chinese" />
      </CheckboxGroup>
    </Form>
  </Fragment>
  );
};

export const run = render(  
  <AdminPage>
    <App />
    <Choose />
  </AdminPage>
);