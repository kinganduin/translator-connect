import ForgeUI, { render, Fragment, Text, Button, IssuePanel, ButtonSet, useState, useProductContext } from "@forge/ui";
import api from "@forge/api";

// API docs: https://docs.microsoft.com/en-au/azure/cognitive-services/translator/reference/v3-0-translate
const TRANSLATE_API = 'https://api.cognitive.microsofttranslator.com/translate?api-version=3.0';
// See README.md for details on generating a Translation API key
const { TRANSLATE_API_KEY, DEBUG_LOGGING } = process.env;
const key = '49a177a2f67b4be3ae93e6f305e8f8c1';
const region = 'eastus';
// See children of the 'dictionary' property from the response from the 
// following URL for valid language codes:
//
//   https://api.cognitive.microsofttranslator.com/languages?api-version=3.0
//
// LANGUAGES is an Array of ['Button text', 'Language code']. 
const LANGUAGES = [ 
  ['🇩🇪 German', 'de'], 
  ['🇬🇧 English', 'en'],
  ['🇫🇷 French', 'fr'], 
  ['🇪🇸 Spanish', 'es'], 
  ['🇨🇳 Chinese', 'zh-Hans'],
];

// Function creates an array of words which will be counted later on
function countWords(sentence) {
  var index = {},
      words = sentence
              .replace(/[.,?!;()"'-]/g, " ")
              .replace(/\s+/g, " ")
              .toLowerCase()
              .split(" ");

    words.forEach(function (word) {
        if (!(index.hasOwnProperty(word))) {
            index[word] = 0;
        }
        index[word]++;
    });

    return index;
}

const Panel = () => {
  // Get the context issue key
  const { platformContext: { issueKey } } = useProductContext();
  // Set up a state object to hold translations
  const [translation, setTranslation] = useState(null);  
  
  async function setLanguage(countryCode) {
    // Fetch issue fields to translate from Jira
    const issueResponse = await api.asApp().requestJira(`/rest/api/2/issue/${issueKey}?fields=summary,description`);
    await checkResponse('Jira API', issueResponse);
    const { summary, description } = (await issueResponse.json()).fields;

    // Translate the fields using the Azure Cognitive Services Translation API
    const translateResponse = await api.fetch(`${TRANSLATE_API}&to=${countryCode}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Ocp-Apim-Subscription-Key': key,
        'Ocp-Apim-Subscription-Region': region,
      },
      body: JSON.stringify([{Text:summary}, {Text:description || 'No description'}])
    });

    // Count words and send them to the console
    var header = Object.values(countWords(summary)).length;
    var body = Object.values(countWords(description)).length;
    //Print to console
    console.log(header + body);

    await checkResponse('Translate API', translateResponse);
    const [summaryTranslation, descriptionTranslation] = await translateResponse.json();

    // Update the UI with the translations
    setTranslation({
      to: countryCode,
      summary: summaryTranslation.translations[0].text,
      description: descriptionTranslation.translations[0].text
    });
  }
  // Render the UI
  return (
    <Fragment>
      <ButtonSet>
        {LANGUAGES.map(([label, code]) => 
          <Button 
            text={label} 
            onClick={async () => { await setLanguage(code); }} 
          />
        )}
      </ButtonSet>
      {translation && (        
        <Fragment>
          <Text content={`**${translation.summary}**`} />
          <Text content={translation.description} />
        </Fragment>
      )}
    </Fragment>
  );
};

/**
 * Checks if a response was successful, and log and throw an error if not. 
 * Also logs the response body if the DEBUG_LOGGING env variable is set.
 * @param apiName a human readable name for the API that returned the response object
 * @param response a response object returned from `api.fetch()`, `requestJira()`, or similar
 */
async function checkResponse(apiName, response) {
  if (!response.ok) {
    const message = `Error from ${apiName}: ${response.status} ${await response.text()}`;
    console.error(message);
    throw new Error(message);
  } else if (DEBUG_LOGGING) {
    console.debug(`Response from ${apiName}: ${await response.text()}`);
  }
}
 
export const run = render(  <IssuePanel>
                              <Panel />
                            </IssuePanel>
                          );

